package autopark.service.impl;

import autopark.dao.IAnnouncementDAO;
import autopark.dao.ICarDAO;
import autopark.dao.ICommentDAO;
import autopark.domain.Car;
import autopark.domain.Comment;
import autopark.domain.CommentEntityEnum;
import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.dto.CommentDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
@Service("carServiceHibernate")
public class AnnouncementServiceImpl implements ICarService{

    @Autowired
    private IAnnouncementDAO carDAO;

    @Autowired
    private ICommentDAO commentDAO;

    public List<CarDTO> getCars() {
        List<Car> cars = carDAO.getCarsHibernate();
        List<CarDTO> resultList = null;
        if(cars != null){
            resultList = new ArrayList<CarDTO>();
            for(Car car: cars){
                resultList.add(DTOAssembler.assembleCarDTO(car));
            }
        }
        return resultList;
    }

    public CarDTO getCar(Long id) {
        Car car = carDAO.get(id);
        CarDTO dto = DTOAssembler.assembleCarDTO(car);

        List<Comment> comments = commentDAO.findComments(CommentEntityEnum.car,id);
        if(comments!=null){
            List<CommentDTO> comlist = new ArrayList<CommentDTO>();
            for(Comment comment : comments){
                comlist.add(DTOAssembler.assembleCommentDTO(comment));
            }
            dto.setComments(comlist);
        }
        return dto;
    }

    @Override
    public List<CarDTO> searchCars(CarSearchDTO dto) {
        return null;
    }

    public List<CarDTO> getMyCars() {
        return null;
    }
}
