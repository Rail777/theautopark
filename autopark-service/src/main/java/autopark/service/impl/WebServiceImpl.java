package autopark.service.impl;

/**
 * Created by aro on 27.03.2016.
 */

import autopark.AutoparkRepositoryException;
import autopark.http.impl.HttpRepositoryImpl;
import autopark.service.IWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebServiceImpl implements IWebService {
    private final Logger log = LoggerFactory.getLogger(WebServiceImpl.class);

    @Autowired
    private HttpRepositoryImpl httpRepository;

    @Override
    public String getMyIp() throws AutoparkRepositoryException {
        return httpRepository.callHttpGet("http://checkip.amazonaws.com");
    }
}
