package autopark.service.impl;

import autopark.dao.IPasswordResetTokenDAO;
import autopark.domain.PasswordResetToken;
import autopark.dto.PasswordResetTokenDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.ITokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 21.03.2016.
 */

@Service
public class ITokenServiceImpl implements ITokenService {

    /*@Autowired
    private PasswordResetTokenJpaRepository tokenRepository;*/
    @Autowired
    private IPasswordResetTokenDAO tokenDAO;


    public boolean isValidToken(String tokenValue) throws AutoparkServiceException {
        /*PasswordResetToken token = tokenDAO.findOneByTokenValue(tokenValue);*/
        PasswordResetToken token = tokenDAO.findOneByTokenValue(tokenValue);
        if (token != null) {

            Date expirationDate = token.getDate();
            Date now = new Date();

            if (expirationDate.before(now)) {
                throw new AutoparkServiceException("Token expired");
            }
            return true;
        }

        return false;
    }

    private PasswordResetTokenDTO assembleDTO(PasswordResetToken passwordResetToken) {

        PasswordResetTokenDTO tokenDTO = new PasswordResetTokenDTO();
        tokenDTO.setTokenDate(passwordResetToken.getDate());
        tokenDTO.setTokenValue(passwordResetToken.getTokenValue());
        return tokenDTO;
    }


}
