package autopark.service;

import autopark.service.exception.RecaptchaServiceException;

/**
 * 01.04.2016.
 */
public interface RecaptchaService {

    boolean isResponseValid(String ipAdress, String response) throws RecaptchaServiceException;
}
