<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>


<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="tiles/head.jsp" %>


        <link rel="stylesheet" type="text/css" href="/styles/announcement.css" />
        <script src="/js/announcement.js"></script>

    </head>


    <body>
        <tags:headerV2 activeAnn="active"/>
        <div id="wrap">
            <div class="container">
                <br/>
                <span class="title2">Объявления</span>
                <div>
                    <div>
                        <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Марка</th>
                        <th>Модель</th>
                        <th>Тип</th>
                        <th><fmt:message key="loading.capacity"/></th>
                        <th>Год выпуска</th>
                        <th>Описание</th>
                        <th>Имя Владельца</th>
                        <th>Email Владельца</th>
                        <th>Телефон Владельца</th>
                        <th>Цена</th>
                        <th>Цена за</th>
                        <th>Просмотр</th>
                    </tr>
                    </thead>
                    <c:if test="${cars != null}">
                        <tbody>
                                <c:forEach items="${cars}" var="item"  varStatus="s">
                                           <tr>
                                               <td>
                                                   <label>
                                                       <a href="/car/${item.id}">${item.id}</a>
                                                   </label>
                                               </td>
                                               <td>
                                                   <label>${item.vendor}</label>
                                               </td>
                                               <td>
                                                   <label>${item.model}</label>
                                               </td>
                                               <td>
                                                   <label>${item.type}</label>
                                               </td>
                                               <td>
                                                   <label>${item.capacity}</label>
                                               </td>
                                               <td>
                                                   <label>${item.year}</label>
                                               </td>
                                               <td>
                                                   <label>${item.description}</label>
                                               </td>
                                               <td>
                                                   <label>${item.user.name} </label>
                                               </td>
                                               <td>
                                                   <label>${item.user.email}</label>
                                               </td>
                                               <td>
                                                   <label>${item.user.phone}</label>
                                               </td>
                                               <td>
                                                   <c:if test="${item.priceDTO.price!=null}">
                                                       <label>${item.priceDTO.price} <fmt:message key="the.currency"/> </label>
                                                   </c:if>
                                               </td>
                                               <td>
                                                   <c:if test="${item.priceDTO.price!=null}">
                                                       <label><fmt:message key="${item.priceDTO.priceUnit}"/></label>
                                                   </c:if>
                                               </td>
                                               <td>
                                                   <a href="/car/${item.id}"><img width="16" height="16" src="/img/arrow1.png"></a>
                                               </td>
                                           </tr>
                                       </c:forEach>
                        </tbody>
                    </c:if>
                </table>
                        </div>
                    </div>
            </div>
        </div>
        <hr>
        <%@ include file="tiles/footer.jsp" %>
    </body>

</html>






