package autopark.dao;


import autopark.domain.Announcement;
import autopark.dto.AnnouncementDTO;

import java.util.List;

public interface IAnnouncementDAO extends IRootDAO<Announcement> {
    List<AnnouncementDTO> getAnnouncements();
    }