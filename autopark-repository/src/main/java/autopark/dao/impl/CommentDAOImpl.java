package autopark.dao.impl;

import autopark.dao.ICommentDAO;
import autopark.domain.Comment;
import autopark.domain.CommentEntityEnum;
import autopark.domain.User;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Repository
public class CommentDAOImpl extends RootDAOImpl<Comment> implements ICommentDAO {


    public CommentDAOImpl() {
        super("autopark.domain.Comment", Comment.class);
    }

    private static final String FIND_COMMENTS = "from Comment where entity = ? and entityId = :entityId order by creationDate desc ";

    @Override
    public List<Comment> findComments(CommentEntityEnum entityEnum, Long id) {
        final Query query = getSession().createQuery(FIND_COMMENTS);
        query.setString(0, entityEnum.name());
        query.setLong("entityId", id);
        return query.list();
    }
}