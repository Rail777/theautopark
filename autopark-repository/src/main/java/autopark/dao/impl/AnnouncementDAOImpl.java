package autopark.dao.impl;

import autopark.dao.IAnnouncementDAO;
import autopark.domain.*;
import autopark.dto.AnnouncementDTO;
import autopark.dto.CarDTO;
import autopark.dto.PriceDTO;
import autopark.rawconnection.Connections;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Repository
public class AnnouncementDAOImpl extends RootDAOImpl<Announcement> implements IAnnouncementDAO {

    public List<AnnouncementDTO> getAnnouncements() {
        List<AnnouncementDTO> result = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Connection connection = Connections.getJNDIConnection();
        try {
            preparedStatement = connection.prepareStatement(
                    "select " +
                            "  C.id as announcementId," +
                            " C.creationDate as announcementCreationDate,"+
                            "  C.description as announcementDescription," +
                            "  C.model as announcementModel," +
                            "  C.price as announcementPrice," +
                            "  C.user_id as announcementUserId" +
                            "  from ap_announcement C ");
           rs = preparedStatement.executeQuery();

            if (rs != null) {
                result = new ArrayList<AnnouncementDTO>();
                while (rs.next()) {
                    AnnouncementDTO announcementDTO = new AnnouncementDTO();
                    //announcementDTO.setId(rs.getLong("announcementId"));

                    announcementDTO.setId(rs.getLong("announcementId"));
                    announcementDTO.setModel(rs.getString("announcementModel"));
                    announcementDTO.setDescription(rs.getString("announcementDescription"));
                    announcementDTO.setCreationDate(rs.getTimestamp("carCreationDate"));

                    BigDecimal price = rs.getBigDecimal("price");

                    result.add(announcementDTO);
                }
            }
            return result;
        } catch (SQLException e) {
            logger.error("Failed to execute a query", e);
        } finally {
            closeAfterRaw(connection, preparedStatement, rs);
        }
        return null;
    }
    }

}
