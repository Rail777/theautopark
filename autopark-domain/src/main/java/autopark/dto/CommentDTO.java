package autopark.dto;

import autopark.domain.CommentEntityEnum;

import java.util.Date;

/**
 * Created by  01 on 21.12.2015.
 */
public class CommentDTO{
    private String description;
    private Date creationDate;
    private CommentEntityEnum entity;
    private Long entityId;
    private String commentatorName;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public CommentEntityEnum getEntity() {
        return entity;
    }

    public void setEntity(CommentEntityEnum entity) {
        this.entity = entity;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getCommentatorName() {
        return commentatorName;
    }

    public void setCommentatorName(String commentatorName) {
        this.commentatorName = commentatorName;
    }
}

