package autopark.dto;

import autopark.domain.CarLoadingCapacityEnum;
import autopark.domain.CarTypeEnum;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by  01 on 21.12.2015.
 */
public class DemandDTO {
    private Long id;
    private CarTypeEnum type;
    private CarLoadingCapacityEnum capacity;
    private String description;
    private Date creationDate;
    private Date startWorkDate;
    private UserDTO userDTO;
    private BigDecimal settlement;

    public CarTypeEnum getType() {
        return type;
    }

    public void setType(CarTypeEnum type) {
        this.type = type;
    }

    public CarLoadingCapacityEnum getCapacity() {
        return capacity;
    }

    public void setCapacity(CarLoadingCapacityEnum capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getStartWorkDate() {
        return startWorkDate;
    }

    public void setStartWorkDate(Date startWorkDate) {
        this.startWorkDate = startWorkDate;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public BigDecimal getSettlement() {
        return settlement;
    }

    public void setSettlement(BigDecimal settlement) {
        this.settlement = settlement;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
