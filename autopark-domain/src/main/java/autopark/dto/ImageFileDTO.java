package autopark.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by aro on 09.04.2016.
 */
public class ImageFileDTO {
    private String fileName;
    private Long id;
    private MultipartFile mpf;
    private String pathfolder;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MultipartFile getMpf() {
        return mpf;
    }

    public void setMpf(MultipartFile mpf) {
        this.mpf = mpf;
    }

    public String getPathfolder() {
        return pathfolder;
    }

    public void setPathfolder(String pathfolder) {
        this.pathfolder = pathfolder;
    }
}
