package autopark.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by  01 on 21.12.2015.
 */
@Entity
@Table(name = "ap_price")
public class Price extends Root {
    private Car car;
    private Date date;
    private BigDecimal price;
    private PriceUnitEnum priceUnit;


    @ManyToOne
    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Enumerated(EnumType.STRING)
    public PriceUnitEnum getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(PriceUnitEnum priceUnit) {
        this.priceUnit = priceUnit;
    }
}
