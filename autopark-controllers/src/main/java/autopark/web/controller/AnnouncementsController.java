package autopark.web.controller;

import autopark.dto.CarDTO;
import autopark.service.ICarService;
import autopark.service.IWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
public class AnnouncementsController {
    @RequestMapping(value = {"/announcements"})
    public String handle(ModelMap modelMap) {
        return "/WEB-INF/content/announcements.jsp";
    }


}



