package autopark.web.utils;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aro on 01.04.2016.
 */
@Component
public class WebHelper {


    public String assembleBaseURL(HttpServletRequest request) {
        return new StringBuilder().
                append(request.getScheme()).
                append("://").append(request.getServerName()).
                append(":").append(request.getServerPort()).
                append(request.getContextPath()).
                toString();
    }

}
